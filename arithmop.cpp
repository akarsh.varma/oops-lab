#include<iostream>
using namespace std;
int main() {

    double num1,num2;
    int choice;

    cout << "Enter two numbers: ";
    cin >> num1 >> num2 ;
    cout << "Enter your choice (1 = addition, 2 = subtration, 3 = multiplication, 4 = division): ";
    cin >> choice;
    switch(choice) {
     case 1:
        cout << num1 + num2;
        break;
     case 2:
        cout << num1 - num2;
        break;
     case 3:
        cout << num1 * num2;
        break;
     case 4:
        if(num2 == 0) {
            cout<<"Erro: Division by zero.";
        } else {
            cout << num1 / num2;
        }
        break;
    default:
       cout << "Invalid choice.";
       break;
    }
    return 0;
}